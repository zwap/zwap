var hostname;
var localhost = "http://localhost:80";
var server = "http://zwap.com.mx";

if(window.location.hostname) {
    hostname = server;
} else {
    hostname = localhost;
}

$(document).ready(function() {

    // Session
    $.get(hostname + "/session", function(data){
        $(".name").html(data["username"]);
    

        $(".zwap-it").click(function(){
            
            query = {};
            
            query["category"] = $(".category-droplist").val();
            query["email"] = data["email"];
            query["name"] = $("input[name=title]").val();
            query["description"] = $("textarea[name=description]").val();
            
            console.log(query);
            
            $.post(hostname + "/products/addZwap", query, function(data){
                
                setTimeout(function(){
                    window.location = hostname + "/zwap/index.html?category=index";
                }, 1000);
                
            }, "json");
            
        });
        
    }, "json");
    
});