var hostname;
var localhost = "http://localhost:80";
var server = "http://zwap.com.mx";

if(window.location.hostname) {
    hostname = server;
} else {
    hostname = localhost;
}

function redirectMe(id, type){

    $.ajax({
        url: hostname + '/products/current/' + id,
        type: "PUT",
        dataType:'json',
        success: function(data){
        console.log(data); }
    }); 
    
    if (type == "item") 
        window.location = hostname + "/zwap/page_item.html";
    else 
        window.location = hostname + "/zwap/page_item-zwap.html";
}

$(document).ready(function() {
    
    $("#noZwaps").hide();
    
    // loadCards()
    function loadCards(zwaps) {
        
        var len = zwaps.length;
        
        for (var i=0; i<len; i++){
            var megaCard = '<div class="card card-md" id="card"><a class="redirect" onclick="redirectMe(\'' + zwaps[i]["id"] + '\',\'item\')"><img class="image-md card-md" src="assets/img/4.png" alt="Item"></a><div class="container card-md"><a class="redirect" onclick="redirectMe(\'' + zwaps[i]["id"] + '\',\'item\')"><h4><b class="card-username">' + zwaps[i]["name"] + '</b></h4></a> <p>' + zwaps[i]["description"] + '</p><div class="card-md-info-div"><input type="button" value="More info...." class="card-md-info"></div><br> <a><img class="star like" onclick="like()" src="assets/img/like.png"></a><a href="#modal" data-toggle="modal"><img  src="assets/img/comment.png" class="comment"></a><a class="redirect" onclick="redirectMe(\'' + zwaps[i]["id"] + '\',\'zwap\')"><img class="zwap" src="assets/img/zwap.png"></a></div></div>';
            $("#MegaCard").append(megaCard);
        }
    }
    
    // Session
    $.get(hostname + "/session", function(data){

        console.log(data);
        email = data["email"];
        
        if(data["session"]) {
            // User data
            $(".name").html(data["username"]);
            // Zwap data
            $("#totalZwaps").html(data["completed"] + data["notDone"]);
            $("#completed").html(data["completed"]);
            $("#notDone").html(data["notDone"]);
            
             // Zwaps
            $.get(hostname + "/products/loadZwaps/" + email, function(zwaps){
                
                console.log();
                numZwaps = zwaps.length;
                    
                if (numZwaps == 0){
                    $("#noZwaps").fadeIn();
                    $("#seeAllZwaps").hide();
                } 
                
                if(numZwaps == 1 || numZwaps == 2) {
                    $(".awards").attr("style", "height: 600px");
                }

                loadCards(zwaps);
                
            }, "json");

        } 

    }, "json"); // End Session

});