var hostname;
var localhost = "http://localhost:80";
var server = "http://zwap.com.mx";

if(window.location.hostname) {
    hostname = server;
} else {
    hostname = localhost;
}

$(document).ready(function() {
    
    $("#login-error").hide();
    $("#create-error").hide();
    
    /* Login */
    $("#login").click(function(){
        
        $("#login-error").hide();
        
        var query = {};
        query["email"] = $("#signin-email").val();
        query["password"] = $("#signin-password").val();
        
        $.post(hostname + "/login", query, function(data){
            
            if(data["login"]){
                
                setTimeout(function(){
                    window.location = hostname + "/zwap/index.html?category=index";
                }, 1000);
                
            } else {
                $("#login-error").fadeIn();
            }
            
        }, "json");
        
    });

    /* Create */
    $("input[name='create']").click(function(){
        
        $("#create-error").hide();
        var username = $("#normalUser").val();
        var email = $("#normalEmail").val();
        var password = $("#normalPass").val();

        var json = {};
        json["username"] = username;
        json["email"] = email;
        json["password"] = password;

        $.post(hostname + "/create", json, function(data){
            
                console.log("This is data:");
                console.log(data);
            
                if(data["create"]==false){
                    $("#create-error").fadeIn();
                    $("input[name='create']").attr("data-dismiss","");
                } else {
                    $('#modal').modal('toggle');
                }
            
        }, "json");

    });
    
    
});