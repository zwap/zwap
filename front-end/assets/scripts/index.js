// Get URL Parameter
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

var categories = "user index technology games books tools miscellaneous clothing music";
var categoriesArray = categories.split(" ");

var hostname;
var localhost = "http://localhost:80";
var server = "http://zwap.com.mx";

if(window.location.hostname) {
    hostname = server;
} else {
    hostname = localhost;
}

function redirectMe(id, type){

    $.ajax({
        url: hostname + '/products/current/' + id,
        type: "PUT",
        dataType:'json',
        success: function(data){
        console.log(data); }
    }); 
    
    if (type == "item") 
        window.location = hostname + "/zwap/page_item.html";
    else 
        window.location = hostname + "/zwap/page_item-zwap.html";
}

$(document).ready(function() {
    
    var pathname = window.location.pathname; // Returns path only
    var url = window.location.href;                       // Returns full URL
    
    var validationCategory = false;
    var category = getUrlParameter('category');
    document.getElementById(category).classList.add("active");

    // validationCategory
    for(var i=0; i<=categoriesArray.length; i++)
    {
        if(category==categoriesArray[i]){
            validationCategory = true;
        }
    }
    
    if(validationCategory==false)
    {
        window.location = categoriesArray[1] + ".html?category=index";
    }

    // loadCards()
    function loadCards(product) {
        
        var len = product.length;
        
        for (var i=0; i<len; i++){
            var megaCard = '<div class="card card-bg" id="card"> <a class="redirect" onclick="redirectMe(\'' + product[i]["id"] + '\',\'item\')"><img class="image-bg card-bg" src="assets/img/1.png" alt="Item"></a> <div class="container card-bg"> <div class="decription-bg"><a class="redirect" onclick="redirectMe(\'' + product[i]["id"] + '\',\'item\')"><h4><b class="card-username">'+ product[i]["name"] + '</b></h4></a> <p>' + product[i]["description"]+ '</p> </div><a><img class="star like" onclick="like()" src="assets/img/like.png"></a> <a href="#modal" data-toggle="modal"><img  src="assets/img/comment.png" class="comment"></a> <a class="redirect" onclick="redirectMe(\'' + product[i]["id"] + '\',\'zwap\')"><img class="zwap" src="assets/img/zwap.png"></a> </div> </div>';
            $("#MegaCard").append(megaCard);
        }
    } 
    
    // GET 
    $.get(hostname + "/products/loadCategories/" + category, function(product){

        loadCards(product);

    }, "json");
    
});