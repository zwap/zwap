var hostname;
var localhost = "http://localhost:80";
var server = "http://zwap.com.mx";

if(window.location.hostname) {
    hostname = server;
} else {
    hostname = localhost;
}

$(document).ready(function() { 

    // GET 
    $.get(hostname + "/products/current/", function(data){
        
        console.log(data);
        
        $(".name").html(data[0]["username"]);
        $(".title").html(data[0]["name"]);
        $(".item-description").html(data[0]["description"]);
        
    }, "json");

});