var hostname;
var localhost = "http://localhost:80";
var server = "http://zwap.com.mx";

if(window.location.hostname) {
    hostname = server;
} else {
    hostname = localhost;
}

$(document).ready(function() {
    
    // Session
    $.get(hostname + "/session", function(data){

        if(data["session"]) {
            $(".logged-out").hide();
            $("#logged-in").fadeIn();
            isActive(data["username"]);
        } else {
            $(".logged-out").fadeIn();
            $("#logged-in").hide();
        }
        
        console.log(data["session"]);

    }, "json");
    
    // Logout
    $("#logout").click(function(){

        $.post(hostname + "/logout", function(data){  
            
                setTimeout(function(){
                    window.location = hostname + "/zwap/index.html?category=index";
                }, 1000);
            
        }, "json");
        
    });
    
    // isActive()
    function isActive(username){
        $("#nav-username").html(username);
    }
    
    
});