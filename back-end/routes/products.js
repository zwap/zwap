var express = require('express');
var router = express.Router();
var productsTable = require("../models/products");
var usersTable = require("../models/users");

var current = {};

/* GET :: current */
router.get('/current', function(req, res, next) {
    res.send(current);
});

/* GET */
router.get('/loadCategories', function(req, res, next) {
	var query = {"category": req.body.category};
		productsTable.findAll({where : query}).then(function(events) {            
            if(events){
                res.json( events );
            }
            else{
                console.log ("Error loading categories");
            }
        });				
});

/* GET :: category */
router.get('/loadCategories/:category', function(req, res, next) {
	var query = {"category": req.params.category};
		productsTable.findAll({where : query}).then(function(events) {            
            if(events){
                res.json( events );
            }
            else{
                console.log ("Error loading categories");
            }
        });				
});

/* GET :: email */
router.get('/loadZwaps/:email', function(req, res, next) {
	var query = {"email": req.params.email};
		productsTable.findAll({where : query}).then(function(events) {            
            if(events){
                res.json( events );
            }
            else{
                console.log ("Not found");
            }
        });				
});

/* POST */
router.post('/addZwap', function(req, res, next) {  
    
	var zwap = {			
		"category" : req.body.category,
        "email" : req.body.email,
        "name" : req.body.name,
        "description" : req.body.description
	}
    
    console.log(zwap);

	productsTable.build(zwap).save().then(function(zwapAdded){
        // Sucess 
        response = {"message":"New zwap added!", "added":true };   
        res.send(response);
        
    }).catch(function (err) {
        // Error 
        console.log(err);
    });
    
});

/* PUT */
router.put('/current/:id', function(req, res, next) {
	var id = req.params.id;
    
    var query = {"id": req.params.id};
    productsTable.findAll({where : query}).then(function(product) {            
        if(product){
            current = product;
            getUsername(product[0]["email"]);
        }
        else{
            console.log ("Not found");
        }
    });
    
    function getUsername(email){
        var query = {"email": email};

        usersTable.findOne({where: query}).then(function(data)
        {
            if(data){
                current[0]["username"] = data["username"];
                console.log("um");
                console.log(data[0]["username"]);
                console.log(data["username"]);
            }
            else{
                current[0]["username"] = "BadWolfxX43";
            }
 
        })

    }
    
});

/* DELETE */
router.delete('/event/:id', function(req, res, next) {
	var id = req.params.id;
	
	productsTable.destroy({
	    where: {
	        id : id
	    }
	});
	
});

module.exports = router;