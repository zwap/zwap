var express = require('express');
var router = express.Router();
var lightUserTable = require("../models/lightUser");
var sequelize = require("../mymodules/sql");

/* GET */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* GET :: Gender */
router.get("/gender",function(req,res,next){
    var genderQty = [['Gender', 'Qty']]; 
    sequelize.query("select gender, count(1) as qty from lightUsers group by gender", { type: sequelize.QueryTypes.SELECT})
  .then(users => {
        console.log(users);
        for(user in users){
            genderQty.push([users[user].gender,users[user].qty]);
        }
        res.json(genderQty);   
    // We don't need spread here, since only the results will be returned for select queries
  })
});

/* GET :: Ages */
router.get("/ages",function(req,res,next){
    var ageQty = [['Ages', 'Quantity']];
    var ageTo10 = 0;
    var ageTo20 = 0;
    var ageTo30 = 0;
    var ageTo40 = 0;
    var ageTo50 = 0;
    var ageTo60 = 0;
    sequelize.query("select age, count(1) as qty from lightUsers group by age", { type: sequelize.QueryTypes.SELECT})
  .then(users => {
  	for(user in users){
  		if(users[user].age<10){
            ageTo10= ageTo10+users[user].qty;
  		}
        if(users[user].age<20 && users[user].age>=10){
            ageTo20= ageTo20+users[user].qty;
        }
        if(users[user].age<30 && users[user].age>=20){
            ageTo30= ageTo30+users[user].qty;
            console.log(ageTo30);
        }
        if(users[user].age<40 && users[user].age>=30){
            ageTo40= ageTo40+users[user].qty;
        }
        if(users[user].age<50 && users[user].age>=40){
            ageTo50= ageTo50+users[user].qty;
        }
        if(users[user].age<60 && users[user].age>=50){
            ageTo60= ageTo60+users[user].qty;
        }
    }
    
    	ageQty.push(['0-10', ageTo10]);
    	ageQty.push(['10-20',ageTo20]);
    	ageQty.push(['20-30',ageTo30]);
    	ageQty.push(['30-40',ageTo40]);
    	ageQty.push(['40-50',ageTo50]);
    	ageQty.push(['50-60',ageTo60]);
        res.json(ageQty);
        
    // We don't need spread here, since only the results will be returned for select queries
  })
    
});

module.exports = router;
