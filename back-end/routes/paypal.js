var express = require('express');
var router = express.Router();
var paypal = require('paypal-rest-sdk');
var tablaPayment = require('../models/paypal');

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AXoj0UaqZbFAN6rU2SBoy8NNwfYltKkFNiir3pYoukwsISpIaMuNZwCSZKhKW0a1F-E0ev7dVwFbzv4m',
  'client_secret': 'EEGpt0JX3ZFVfdKXsWJeC4T_1uaDsHmm6VMrJL39z3wnU-KJ-kCX3bkNkEBdG_4OLYGpiDT2AcqPH4KA'
});

/* POST */
router.post('/paypal_create', function(req, res, next) {
	console.log(req.body);
    console.log(req.body.paypalpayment);
    
    var create_payment_json = {
	    "intent": "sale",
	    "payer": {
	        "payment_method": "paypal"
	    },
	    "redirect_urls": {
	        "return_url": "http://zwap.com.mx/paypal/success",
	        "cancel_url": "http://zwap.com.mx/paypal/error"
	    },
	    "transactions": [{	       
	        "amount": {
	            "currency": "MXN",
	            "total": req.body.paypalpayment
	        },
	        "description": "This is the payment description."
	    }]
	};

    console.log(create_payment_json);
     
    // Payment Create
    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            console.log(error);
            throw error;
        } else {
            console.log("Create Payment Response");
            console.log(payment);
            var paymentData = {
                paypalCreate : create_payment_json,
                paypalCreateResponse : payment,
                payPalPaymentID : payment.id,
                qty : req.body.paypalpayment,
                status : "created"
            };
            tablaPayment.build(paymentData).save().then(function(){		
                res.redirect(payment.links[1].href)
            }).catch(function(err){
                console.log(err);
            });
        }
    });
    
});

/* GET :: Sucess */
router.get('/success', function(req, res, next) { 
	var paymentData = {
		payerId : req.query.PayerID,
		paypalToken : req.query.token,
		status : "completed"
	};

	tablaPayment.update(paymentData, {
        where: {
            payPalPaymentID : req.query.paymentId
        }
    }).then(function(){
        res.redirect('/zwap/confirm_donation.html');
    });
    
});

/* GET :: Error */
router.get('/error', function(req, res, next) { 
	var paymentData = {
		status : "failed"
	};

	tablaPayment.update(paymentData, {
        where: {
            payPalPaymentID : req.query.paymentId
        }
    }).then(function(){

		res.send("Paypal Error");
    });
});

module.exports = router;