var express = require('express');
var router = express.Router();
var tablaUsers = require("../models/users");
var rest = require("../mymodules/rest");
var hbsmailer = require('../mymodules/hbsmailer'); 
var sha256 = require('sha256'); 

/* GET */
router.get('/', function(req, res, next) {
  res.redirect('/zwap/index.html?category=index');
});

/* GET */
router.get('/confirm_email', function(req, res, next) {
  res.redirect('/zwap/confirm_email.html');
});

/* POST */
router.post('/create', function(req, res, next){
     
    var query = {"email": req.body.email};

	tablaUsers.findOne({where: query}).then(function(email)
	{
		if(email){
			responselogin = {"message":"That email is already registered.", "create":false}
			res.json(responselogin);
		}
		else{
            
            var NewUser={
                // User data
                "username" : req.body.username,		
                "email" : req.body.email,		
                "password" : sha256(req.body.password),
                // Zwap data
                "completed" : 0, 
                "notDone" : 0
            }
            create(req, res, NewUser);
		}
		
	})
              
});

/* CREATE */
function create(req, res, newUser){
 
    tablaUsers.build(newUser).save().then(function(new_user){

      // ** HBS Mailer **
      hbsmailer.sendMail({
           from: 'zwap.com.mx@gmail.com',
           to: req.body.email,
           subject: 'Welcome ' + req.body.username,
           template: 'lol',
           context: {
                username : req.body.username,
                confirm_link : 'http://zwap.com.mx/confirm_email'
           }
       }, function (error, response) {
           console.log(error);
           console.log(response);
           console.log('mail sent');
           hbsmailer.close();
       }); // ** end mailer ** 

    createJson = {"message":"A new user has been created!", "create":true };   
    res.send(createJson);
        
    }).catch(function (err) {
        // Error!! :(
        console.log(err);
		createJson = {"message":"An error has occured!", "create":false };  				 
        res.send(createJson);
    });
    
}

/* POST */
router.post('/login', function(req, res, next) {		
	var responselogin = {};
	console.log(req.body);	

	var query = {"email":req.body.email, "password":sha256(req.body.password)};

	tablaUsers.findOne({where: query}).then(function(user)
	{
		if(user){
			responselogin = {"message":"Login exitoso", "login":true}
            req.session.user = user;
		}
		else{
			responselogin = {"message":"Login fallido", "login":false}
		}
		res.json(responselogin);
	})
});

/* POST */
router.post('/logout', function(req, res, next) {	
    
    var responselogout = {};
    req.session.destroy(function(err) {
        responselogout = {"message":"Logout exitoso", "logout":true}
        console.log("Logout successful");
    })
    
    res.json(responselogout);
});

/* GET */
router.get('/session', function(req, res, next){
   /*
    res.json({"username": "Mauricios", 
                  "email": "mauricios@mailinator.com", 
                  "session": true, 
                  "completed": 0, 
                  "notDone": 0
                 });  
    */

	if(req.session && req.session.user){
        req.session.user["session"] = true;
        res.json(req.session.user);
	}
	else{
		res.json({"message:": "User non existent", "session": false});
	}


});

/* **** FACEBOOK **** */

/* GET */
router.get('/facebook_redirect', function(req, res, next) {
	// Capturar datos de facebook 	
	var redirect_uri = "http://zwap.com.mx/facebook_redirect";
	var facebook_code = req.query.code;
	var accesstoken_call = {
	    host: 'graph.facebook.com',
	    port: 443,
	    path: '/v2.10/oauth/access_token?client_id=321356621663563&redirect_uri='+redirect_uri+ //Nuestro id 
	    '&client_secret=0b47dc5e26b9b68823993e07197c32f9&code='+facebook_code, //Nuestro client_secret
	    //Requisito de email y que regrese requisitos
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json'
	    }
	};
	console.log(accesstoken_call);
	rest.getJSON(accesstoken_call, function(statusCode, access_token_response) {	    
	    var FB_path = '/me?fields=id,name,email,picture&access_token='+access_token_response.access_token;
	    console.log("token: " + access_token_response.access_token);
	    console.log("FB_path: " + FB_path);
	    var userinfo_call = {
		    host: 'graph.facebook.com',
		    port: 443,
		    path: FB_path,
		    method: 'GET',
		    headers: {
		        'Content-Type': 'application/json'
		    }
		};
		rest.getJSON(userinfo_call, function(statusCode, user_info_response) {	    	
	    	console.log(user_info_response);
			tablaUsers.find( { where : { "facebookID" : user_info_response.id } } ).then(function(user){ //facebookID en tabla de usuarios y facebook access token
				if(user){
					console.log("Existent user");
				}
				else{
					var newUserData = {					//Las cosas que ingresa el usuario al crear su acc.
						"username":user_info_response.name,
						"email":user_info_response.email,
						"facebookID":user_info_response.id,
						"facebook_access_token":facebook_code,
						
					}
				
				console.log("Creando Usuario");
				console.log(newUserData);

				tablaUsers.build(newUserData).save().then(function(){
					createJson = {"message":"User created succesfully", "created":true};
				}).catch(function(err){
					console.log("Error creating user");
					createJson = {"message":"Failed to create user", "created":false};
				});}
			});
			res.redirect("http://zwap.com.mx/zwap/page_user-profile.html");  
		});
	});
});

/* POST */
router.post('/loginFB', function(req, res, next) {
	var redirect_uri = "http://zwap.com.mx/facebook_redirect";
	res.redirect("https://www.facebook.com/v2.10/dialog/oauth?scope=email,public_profile&client_id=321356621663563&redirect_uri="+redirect_uri);  
});

module.exports = router;
