var hbs = require('nodemailer-express-handlebars');
var nodemailer = require('nodemailer');

var options = {
     viewEngine: {
         extname: '.hbs',
         layoutsDir: 'views/'
     },
     viewPath: 'views/',
     extName: '.hbs'
};
// var sgTransport = require('nodemailer-sendgrid-transport');

var gmail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'zwap.com.mx@gmail.com',
        pass: '123Fer987Fer'
    }
});

// var mailer = nodemailer.createTransport(sgTransport(gmail));
gmail.use('compile', hbs(options));

module.exports = gmail;