var Sequelize = require('sequelize');
var mysql_host = "localhost";

var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = null; 
}

var sequelize = new Sequelize('basedatos', 'root', mysql_pass, { 
  host: mysql_host,
  dialect: 'mysql'
});

var users = sequelize.define('users', {
    // Facebook data
    "facebookID" : {type: Sequelize.TEXT},
    "facebook_access_token" : {type: Sequelize.TEXT},
    "userid": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
	// User data
    "username" : { type: Sequelize.STRING },
    "email" : { type: Sequelize.STRING },
    "password" : { type: Sequelize.STRING },
    "country" : { type: Sequelize.STRING },
    "state" : { type: Sequelize.STRING },
    "city" : { type: Sequelize.STRING },
    "interests" : { type: Sequelize.JSON},
    // Zwap data
    "completed" : { type: Sequelize.INTEGER },
    "notDone" : { type: Sequelize.INTEGER },
    "rating" : { type: Sequelize.INTEGER },
    // System data
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING }
}); 

users.sync();
module.exports = users;