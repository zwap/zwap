var Sequelize = require('sequelize');
var mysql_host = "localhost";

var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = null; 
}

var sequelize = new Sequelize('basedatos', 'root', mysql_pass, { 
  host: mysql_host,
  dialect: 'mysql'
});

var payments = sequelize.define('payments', {
    "paymentid": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    "payerId" : { type: Sequelize.STRING },
	"payerToken" : { type: Sequelize.STRING },
    "paypalCreate": { type: Sequelize.JSON },
    "paypalCreateResponse": { type: Sequelize.JSON },
    "payPalPaymentID": { type: Sequelize.STRING },
    "qty" : { type: Sequelize.STRING },
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
}); 

payments.sync();
module.exports = payments;