var Sequelize = require('sequelize');
var mysql_host = "localhost";

var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = null; 
}

var sequelize = new Sequelize('basedatos', 'root', mysql_pass, { 
  host: mysql_host,
  dialect: 'mysql'
});

var lightUser = sequelize.define('lightUser', {
    "id" : { type: Sequelize.INTEGER, primaryKey: true },
    "name" : { type: Sequelize.STRING},
    "age" : { type: Sequelize.INTEGER },
    "gender" : { type: Sequelize.STRING },
}); 

lightUser.sync();
module.exports = lightUser;
