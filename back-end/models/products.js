var Sequelize = require('sequelize');
var mysql_host = "localhost";

var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = null; 
}

var sequelize = new Sequelize('basedatos', 'root', mysql_pass, { 
  host: mysql_host,
  dialect: 'mysql'
});

var products = sequelize.define('products', {
    "id" : { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    "likes" : { type: Sequelize.INTEGER},
    "category" : {type: Sequelize.STRING},
    "email" : { type: Sequelize.STRING },
    "username" : { type: Sequelize.STRING },
    "name" : { type: Sequelize.STRING },
    "description" : { type: Sequelize.STRING },
    "mainPhoto" : { type: Sequelize.BLOB },
    "otherPhoto" : { type: Sequelize.BLOB },
    "otherPhoto2" : { type: Sequelize.BLOB },
    "otherPhoto3" : { type: Sequelize.BLOB },
    "otherPhoto4" : { type: Sequelize.BLOB },
    "otherPhoto5" : { type: Sequelize.BLOB },
    "comments" : { type: Sequelize.STRING }
});

products.sync();
module.exports = products;