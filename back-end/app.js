var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var sessions = require('express-session');
var logger = require('morgan');
var path = require('path');
var favicon = require('serve-favicon');

// *** Routes
var analytics =require('./routes/analytics');
var index = require('./routes/index');
var paypal =require('./routes/paypal');
var products = require('./routes/products');
var users = require('./routes/users');

var app = express();

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs'); // Changed to hbs

// Allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

app.use(logger('dev'));
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// *** Sessions
app.use(sessions({  
 cookieName: 'session',
 secret: 'YUaxGqaaIExEvIVUSCjc4k7LdWzV02DGaAu8VitrGxs3cgLmAsEmiq0a0OBb9b8we5pbws9qPmk5MVyY',
 duration: 24 * 60 * 60 * 1000,  // ms - un día de duración 
 activeDuration: 1000 * 60 * 5  // 5 min para extender la duración de la sesión
}));

// *** Use
app.use('/zwap', express.static("../front-end")); 
app.use('/', index);
app.use('/products', products);
app.use('/users', users);
app.use('/analytics', analytics);
app.use('/paypal', paypal);

module.exports = app;